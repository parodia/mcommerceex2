package com.apps.j.mcommerceex2;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by j on 04.05.15.
 */
public class DrawerListAdapter extends BaseExpandableListAdapter {
    private Context context;
    private List<String> continents;
    private HashMap<String, List<String>> countries;

    public DrawerListAdapter(Context context, List<String> continents, HashMap<String, List<String>> countries) {
        this.context = context;
        this.continents = continents;
        this.countries = countries;
    }

    @Override
    public int getGroupCount() {
        return this.continents.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return this.countries.get(this.continents.get(groupPosition)).size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this.continents.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return this.countries.get(this.continents.get(groupPosition)).get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {

        if (convertView == null) {
            convertView = new TextView(context);
        }
        ((TextView) convertView).setText(continents.get(groupPosition));

        convertView.setTag(continents.get(groupPosition));
        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        final String childText = (String) getChild(groupPosition, childPosition);
        if (convertView == null) {
            convertView = new TextView(context);
        }
        ((TextView) convertView).setText(">"+childText);
        convertView.setTag(childText);
        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}
