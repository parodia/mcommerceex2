package com.apps.j.mcommerceex2;

import android.view.View;

/**
 * Created by j on 04.05.15.
 */
public interface NLevelView {

    public View getView(NLevelItem item);
}
