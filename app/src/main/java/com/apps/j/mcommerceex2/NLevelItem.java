package com.apps.j.mcommerceex2;

import android.view.View;

/**
 * Created by j on 04.05.15.
 */
public class NLevelItem implements NLevelListItem {

    //private Object wrappedObject;
    private String name;
    private NLevelItem parent;
    private NLevelView nLevelView;
    private boolean isExpanded = false;
    private boolean theYoungest = true;

    public NLevelItem(String name, NLevelItem parent, NLevelView nLevelView) {
        this.name = name;
        this.parent = parent;
        this.nLevelView = nLevelView;
    }

    public String getName() {
        return name;
    }

    public void setTheYoungest(boolean theYoungest) {
        this.theYoungest = theYoungest;
    }

    public boolean isTheYoungest() {
        return theYoungest;
    }

    @Override
    public boolean isExpanded() {
        return isExpanded;
    }
    @Override
    public NLevelListItem getParent() {
        return parent;
    }
    @Override
    public View getView() {
        return nLevelView.getView(this);
    }
    @Override
    public String toggle() {
        isExpanded = !isExpanded;
        return "";
    }
}
