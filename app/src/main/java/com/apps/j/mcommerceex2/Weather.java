package com.apps.j.mcommerceex2;

import java.io.Serializable;

/**
 * Created by j on 04.05.15.
 */

public class Weather implements Serializable{
    String city;
    String main;
    float temperature;
    int humidity;

    public Weather() {
    }

    public Weather(String city, String main, float temperature, int humidity) {
        this.city = city;
        this.main = main;
        this.temperature = temperature - 273.16f;
        this.humidity = humidity;
    }

    @Override
    public String toString() {
        return city+" "+main+" "+temperature+" "+humidity;
    }
}
