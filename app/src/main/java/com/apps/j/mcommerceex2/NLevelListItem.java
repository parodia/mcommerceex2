package com.apps.j.mcommerceex2;

import android.view.View;

/**
 * Created by j on 04.05.15.
 */
public interface NLevelListItem {

    public boolean isExpanded();
    public String toggle();
    public NLevelListItem getParent();
    public View getView();
}
