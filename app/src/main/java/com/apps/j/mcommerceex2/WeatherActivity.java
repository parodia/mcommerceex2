package com.apps.j.mcommerceex2;

import android.app.Activity;

import android.app.ActionBar;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.support.v4.widget.DrawerLayout;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import java.util.Random;


public class WeatherActivity extends Activity
        implements NavigationDrawerFragment.NavigationDrawerCallbacks {



    private NavigationDrawerFragment mNavigationDrawerFragment;

    private CharSequence mTitle;
    private static String cityName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather);

        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getFragmentManager().findFragmentById(R.id.navigation_drawer);
        mTitle = getTitle();

        // Set up the drawer.
        mNavigationDrawerFragment.setUp(
                R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout));

    }

    @Override
    public void onNavigationDrawerItemSelected(int position, String loc) {
        // update the main content by replacing fragments
        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.container, PlaceholderFragment.newInstance(loc))
                .commit();
    }


    public void restoreActionBar() {
        ActionBar actionBar = getActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(mTitle);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!mNavigationDrawerFragment.isDrawerOpen()) {
            getMenuInflater().inflate(R.menu.weather, menu);
            restoreActionBar();
            return true;
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        List<NLevelItem> list;
        ListView listView;
        Weather weather1 = new Weather();
        View view;

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(String city) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putString("city", city);
            fragment.setArguments(args);
            return fragment;
        }

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {

            view = inflater.inflate(R.layout.fragment_weather, container, false);

            final String city = getArguments().getString("city");


            final String url = "http://api.openweathermap.org/data/2.5/weather?q="+city;
            System.out.println(url);
            cityName = city.split(",")[0];


            final Context context = getActivity().getApplicationContext();
            if (((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo() == null) {
                Toast.makeText(context, "No internet connection", Toast.LENGTH_SHORT).show();
                if (ifFileExists(cityName, context)) {
                    weather1 = readFromCache(cityName);
                    System.out.println(weather1);
                    ((TextView) view.findViewById(R.id.desc)).setText(weather1.main);
                    ((TextView) view.findViewById(R.id.temp)).setText("" + weather1.temperature);
                    ((TextView) view.findViewById(R.id.hum)).setText("" + weather1.humidity);
                    Toast.makeText(context, "Read from cache", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(context, "No cached data for "+cityName, Toast.LENGTH_SHORT).show();
                }

            }
            else {

                new AsyncTask<String, Void, String>() {
                    @Override
                    protected String doInBackground(String... params) {
                        return getJSONfromUrl(url);
                    }

                    @Override
                    protected void onPostExecute(String jsonStr) {
                        try {
                            JSONObject jsonObject = new JSONObject(jsonStr);
                            JSONObject weather = jsonObject.getJSONArray("weather").getJSONObject(0);
                            JSONObject main = jsonObject.getJSONObject("main");
                            weather1 = new Weather(city, weather.getString("main"), Float.parseFloat(main.getString("temp")), Integer.parseInt(main.getString("humidity")));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        Toast.makeText(context, "Saved to cache as "+saveToCache(weather1, cityName, context), Toast.LENGTH_SHORT).show();
                        ((TextView) view.findViewById(R.id.desc)).setText(weather1.main);
                        ((TextView) view.findViewById(R.id.temp)).setText("" + weather1.temperature);
                        ((TextView) view.findViewById(R.id.hum)).setText("" + weather1.humidity);
                    }

                }.execute();
            }

            return view;
        }

        @Override
        public void onAttach(Activity activity) {
            Log.i("FRAGMENT", "onAttach");
            super.onAttach(activity);
            ((WeatherActivity)activity).mTitle = cityName;

        }

        private String saveToCache(Object weather, String city, Context context) {
            System.out.println("cache "+context.getExternalCacheDir());
            //String filename = Environment.getExternalStorageDirectory().getAbsolutePath()+"/"+city+".txt";
            String filename = context.getExternalCacheDir()+"/"+city+".txt";
            try {
                OutputStream outputStream = new FileOutputStream(filename);
                OutputStream buffer = new BufferedOutputStream(outputStream);
                ObjectOutput output = new ObjectOutputStream(buffer);
                output.writeObject(weather);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return filename;
        }

        private boolean ifFileExists (String city, Context context) {
            String filename = context.getExternalCacheDir()+"/"+city+".txt";
            File file = new File(filename);
            if (file.exists())
                return true;
            else return false;
        }

        private Weather readFromCache(String city) {
            String filename = city+".txt";
            Weather w = new Weather();
            try {
                InputStream inputStream = new FileInputStream(filename);
                InputStream buffer = new BufferedInputStream(inputStream);
                ObjectInput input = new ObjectInputStream(buffer);
                w = (Weather)input.readObject();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return w;
        }

        private String getJSONfromUrl(String url) {
            InputStream is = null;
            String jsonStr = "";
            try {

                DefaultHttpClient httpClient = new DefaultHttpClient();
                HttpPost httpPost = new HttpPost(url);
                HttpResponse httpResponse = httpClient.execute(httpPost);
                HttpEntity httpEntity = httpResponse.getEntity();
                is = httpEntity.getContent();
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }
            try {
                BufferedReader reader = new BufferedReader(new InputStreamReader(is,"utf-8"),8);
                StringBuilder sb = new StringBuilder();
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line + "\n");
                }
                is.close();
                jsonStr = sb.toString();
            } catch(Exception e) {
                e.printStackTrace();
                return null;
            }
            System.out.println(jsonStr);
            return jsonStr;
        }
    }

}
